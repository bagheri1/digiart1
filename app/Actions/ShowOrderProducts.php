<?php

namespace App\Actions;

use TCG\Voyager\Actions\AbstractAction;

class ShowOrderProducts extends AbstractAction
{
    public function getTitle()
    {
        return 'نمایش محصولات';
    }

    public function getIcon()
    {
        return 'voyager-eye';
    }

    public function getPolicy()
    {
        return 'read';
    }

    public function getAttributes()
    {
        return [
            'class' => 'btn btn-sm btn-success pull-right',
        ];
    }

    public function getDefaultRoute()
    {
        return route('action.show.order.products',$this->data->{$this->data->getKeyName()});
    }

    public function shouldActionDisplayOnDataType()
{
    return $this->dataType->slug == 'orders';
}
}
