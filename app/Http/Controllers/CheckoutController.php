<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Cart;
use Zarinpal\Zarinpal;
use App\Payment;
use App\Order;
use App\Product;
use App\Shipping;

// use Gloudemans\Shoppingcart\Cart as ShoppingcartCart;

class CheckoutController extends Controller
{

    public function index()
    {
        //send categories to this page
         $categories = Category::all();

        return view('checkout')->with([
            'categories'=>$categories,
            'discount' => $this->getNumbers()->get('discount'),
            'newTotal'=>$this->getNumbers()->get('newTotal'),
            'newTax'=>$this->getNumbers()->get('newTax'),
            'newSubTotal'=>$this->getNumbers()->get('newSubTotal'),
          ]);
    }


    public function store(Request $request)
    {
        //save shipping
        $insertedShipping= Shipping::create($request->all());
        //generate product ids for order
        $product_ids=array();
        foreach (Cart::instance('default')->content() as $product){
          array_push($product_ids,$product->id);
        }
        //save order
        $ifExistOrder =Order::where([
            'user_id'=>auth()->user()->id,
            'status'=>'pending'
        ])->first();
        if ($ifExistOrder){
                //update & existance order
                $ifExistOrder-> shipping_id=$insertedShipping->id;
                $ifExistOrder->product_ids=json_encode($product_ids);
                $ifExistOrder->save();
        }else{
            Order::create([
                'user_id'=>auth()->user()->id,
                'shipping_id'=>$insertedShipping->id,
                'payment_id'=>0,
                'status'=>'pending',
                'product_ids'=>json_encode($product_ids),
             ]);
        }

        $this->zarinPalDoPay();
        //payment

        //order information

    }


    public function zarinPalDoPay()
    {
        // zarinpal merchen code
        $zarinpal = new Zarinpal('d469a65e-9997-11e9-8cae-000c29344814');
        // $zarinpal->enableSandbox();

        $results = $zarinpal->request(
           route('checkout.callback'),          //url callback
            $this->getNumbers()->get('newTotal'),      //total amount
            // 1000,
            auth()->user()->name,                                 //name
            auth()->user()->email,                   //email
            auth()->user()->phone                   //phone

        );

        if (isset($results['Authority'])) {

            $ifExist = Payment::where([
                'session_id'=>session()->getId(),
                'status'=>'pending'
                ])->first();

            if ($ifExist){
                $ifExist->price =$this->getNumbers()->get('newTotal');
                $ifExist->authority=$results['Authority'];
                $ifExist->status ='pending';
                $ifExist->refid =0;
                $ifExist->save();
            }else{
                $payment = Payment::create([
                    'authority'=>$results['Authority'],
                    'price'=>$this->getNumbers()->get('newTotal'),
                    'status'=>'pending',
                    'refid'=>0,
                    'session_id'=> session()->getId(),
                ]);
            }

            $zarinpal->redirect();

        }

    }


    //zarinpal callback method
    public function zarinPalCallBack()
    {
        //verify

        echo 'this is ok callback';

                $zarinpal = new Zarinpal('d469a65e-9997-11e9-8cae-000c29344814');
                $authority = Payment::where('session_id',session()->getId())->first()->authority;
                $status=json_encode( $zarinpal->verify('OK', 1000, $authority)['status']);
                if($status=='"verified_before"'||$status=='"success"'){
                    //success payment
                    $refid=json_encode( $zarinpal->verify('OK', 1000, $authority)['RefID']);
                    $successpay= Payment::where('session_id',session()->getId())->first();
                    $payment_id=$successpay->id;
                    $successpay->status='complate';
                    $successpay->refid=$refid;
                    $successpay->save();
                    //update order table to complate
                    $ifExistOrder =Order::where([
                    'user_id'=>auth()->user()->id,
                    'status'=>'pending'
                    ])->first();
                    if($ifExistOrder){
                        $ifExistOrder->statuse='complete';
                        $ifExistOrder->payment_id=$payment_id;
                        $ifExistOrder->save();
                    }

                    //empaty cart
                    Cart::instance('default')->destroy();
                    return redirect()->route('checkout.successful',$refid);
                }else{

                    echo 'not success payment';
                }

        // 'Status'(index) going to be 'success', 'error' or 'canceled'
    }


    private function getNumbers()
    {
        $tax = config('cart.tax')/100 ;
        $discount = session()->get('coupon')['discount'] ??0;
        $newSubTotal = number_format((float)Cart::subtotal(),2)*1000 - $discount;
        $newTax = $newSubTotal * $tax;
        $newTotal = $newSubTotal * (1+$tax);

        return collect([
          'tax'=>$tax,
          'discount'=>$discount,
          'newSubTotal' => $newSubTotal,
          'newTax' => $newTax,
          'newTotal'=>$newTotal
        ]);
   }

   public function successfull($refid=0)
   {
     $categories = Category::all();

     return view('checkout_success')->with([
         'categories'=>$categories,
         'refid'=>$refid,
     ]);
   }


}
