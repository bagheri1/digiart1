<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use Product;

class saveForLaterController extends Controller
{

    public function destroy($id)
    {
      Cart::instance('saveForLater')->remove($id);
      return redirect(route('cart.index'))-> with('success_message',' محصول از لیست ذخیره ها حذف شد');
    }


    public function saveForLater($id)
    {
         //delete from defult cart
         //inser to saveForLater instance Cart
        $isExist = Cart::search(function($cartItem,$rowId) use ($id){
          return $rowId===$id;
        });
        if($isExist->isNotEmpty()){

          $item = Cart::get($id);

          cart::instance('saveForLater')->add($item->id, $item->name, 1 , $item->price)
          ->associate('App\Product');
          // remove from default Cart
          Cart::instance('default')->remove($id);
          return redirect(route('cart.index'))-> with('success_message','محصول به لیست ذخیره ها اضافه شد  ');
        }
    }

    public function  switchFromSavedToCart($id)
    {
      $item = Cart::instance('saveForLater')->get($id);
      Cart::instance('saveForLater')->remove($id);

      $duplicated = Cart::instance('default')->search(function($cartItem,$rowId) use ($id){
        return $rowId===$id;
      });
      if($duplicated->isNotEmpty()){
        return redirect(route('cart.index'))-> with('success_message','محصول از قبل در سبد وجود دارد');
      }else{
        cart::instance('default')->add($item->id, $item->name, 1 , $item->price)
        ->associate('App\Product');
        return redirect(route('cart.index'))-> with('success_message','محصول با موفقیت از لیست ذخیره به سبد اضافه شد');
      }
    }
}
