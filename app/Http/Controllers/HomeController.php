<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use App\Shipping;
use App\Payment;
use Illuminate\Http\Request;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function order_products($id)
    {
        $thisOrder = Order::find($id);
        $product_ids_array = json_decode($thisOrder->product_ids);
        // dd($product_ids_array);
        // $product_ids_new_array= $product_ids_array;
        // $product_ids_array_final= explode(',',$product_ids_new_array);
        $products =array();
        // return $product_ids_array_final;
        foreach($product_ids_array as $product_id){
             // echo $order.'<br>';
          array_push($products,Product::find($product_id))  ;
        };

        //shipping information
        // $shipping_id = $thisOrder ->shipping_id;
        // $shipping = Shipping::find($shipping_id)->first();


        // paymwnt information
        // $payment_id = $thisOrder-> payment_id;
        // $payment = Payment::find($payment_id);




        return view('vendor.voyager.orders.products',[
            'products'=>$products,
            // 'shipping'=>$shipping,
            // 'payment'=>$payment,
        ]);
    }
}
