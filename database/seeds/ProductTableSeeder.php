<?php

use Illuminate\Database\Seeder;
use App\product;
class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    //naghashi category
    public function run()
    {
      product::create([
     'name'=>'بال',
     'slug'=>'bal',
     'details'=>'تابلو نقاشی با سایز 70*50',
     'price'=>'2500000',
     'description'=>'این تابلو مناسب انسان هایی با تفکر عمیقتر نسبت به زندگی است'
   ])
   ->categories()->attach(1);

   product::create([
   'name'=>'زیر سایه',
   'slug'=>'zire-saye',
   'details'=>'تابلو نقاشی با سایز 40*45',
   'price'=>'499000',
   'description'=>'این تابلو سراسر از آرامش است با یاعث ایجاد حس خنکی در روح و جان انسان میشود'
 ])
 ->categories()->attach(1);

 product::create([
 'name'=>'روزمره گی',
 'slug'=>'rozmaregi',
 'details'=>'تابلو نقاشی با سایز40*45',
 'price'=>'2190000',
 'description'=>'این تابلو دارای رنگبندی نقیضی است که ما را مجبور به فکر عمییقتری نسبت به زندگی میکند'
])
->categories()->attach(1);


product::create([
'name'=>'یشم 2',
'slug'=>'yashm2',
'details'=>'تابلو نقاشی با سایز 70*75',
'price'=>3390000,
'description'=>'این تابلو با رنگی ملیح و آرامش دهنده است و مناسب برای خانهاست'
])
->categories()->attach(1);

product::create([
'name'=>'حامی',
'slug'=>'hami',
'details'=>'تابلو نقاشی با سایز 40*45',
'price'=>2590000,
'description'=>'این تابلو دارای رنگبندی بسیار زیباست و با نگاه اول ما را به یاد حامیانمان در زندگی می اندازد'
])
->categories()->attach(1);

product::create([
'name'=>'گلدان روشن',
'slug'=>'goldane-roshan',
'details'=>'تابلو نقاشی با سایز70*40',
'price'=>3190000,
'description'=>'این تابلو نقاشی  مناسب پذیرایی و دفتر کار است '
])
->categories()->attach(1);

product::create([
'name'=>'حوض گل',
'slug'=>'hoz-gol',
'details'=>'ستی ترکیبی از نقاشی و رزین و اثری مدرن است که جنس آن از برنج می باشد و رنگی ثابت دارد',
'price'=>160000,
'description'=>'this is a asar of bagheri'
])
->categories()->attach(5);

product::create([
'name'=>'عشق',
'slug'=>'eshgh',
'details'=>'این اثر نیم ستی است که ترکیبی از نقاشی و نوشته واست و جنس ان برنج است',
'price'=>149000,
'description'=>'this is a naghashi of ...'
]);
product::create([
'name'=>'حوض اسلیمی',
'slug'=>'hoz-eslimi',
'details'=>'نیم ست  مسی',
'price'=>149000,
'description'=>'این اثر تزکیبی از نقاشی و مس است که باعث بوجود آمدن اثری خاص شده است'
]);

product::create([
'name'=>'خاتم سرخ',
'slug'=>'khatam-sorkh',
'details'=>'دستبند خاتم با عقیق سرخ',
'price'=>149000,
'description'=>'این دستبند ترکیبی از خاتم و مهره های عقیق است و مناسب شیک پوشان می باشد'
]);


product::create([
'name'=>'خاتم مشکی',
'slug'=>'katame-meshki',
'details'=>'دستبند خاتم ',
'price'=>149000,
'description'=>'این دستبند ترکیبی از خاتم و مهره های عقیق است و مناسب شیک پوشان می باشد'
]);


product::create([
'name'=>'غروب',
'slug'=>'ghorob',
'details'=>'دستبند غروب',
'price'=>149000,
'description'=>'این دستبند برگرفته از غروب دل انگیز آفتاب است'
]);


product::create([
'name'=>'گل',
'slug'=>'gol',
'details'=>' ست گل با رنگ غالب آبی ',
'price'=>149000,
'description'=>'این ست ترکیبی از نقاشی و رزین است با جنسی از مس گه ترکیبی مدرن و سنتی بسیار شیک تشکیل داده است. '
]);



    }
}
