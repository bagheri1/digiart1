<?php

use Illuminate\Database\Seeder;
use App\Category;

class My_CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
          'name'=>'نقاشی',
          'slug'=>'naghashi'
        ]);
        Category::create([
          'name'=>'خطاطی',
          'slug'=>'khatati'
        ]);
        Category::create([
          'name'=>'مجسمه',
          'slug'=>'mojasame'
        ]);
        Category::create([
          'name'=>'چرم',
          'slug'=>'charm'
        ]);
        Category::create([
          'name'=>'زیورآلات',
          'slug'=>'zivaralat'
        ]);
        Category::create([
          'name'=>'چوب',
          'slug'=>'choob'
        ]);

    }
}
