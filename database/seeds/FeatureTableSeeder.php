<?php

use Illuminate\Database\Seeder;
use App\Feature;

class FeatureTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Feature::create([
          'name'=>'تابلو نقاشی',
          'value'=>'naghashi',
          'product_id'=>27
        ]);
        Feature::create([
          'name'=>'کلاس سایزی',
          'value'=>'معمولی| 40*30',
          'product_id'=>27
        ]);
        Feature::create([
          'name'=>'مناسب برای',
          'value'=>'خانه و محل کار',
          'product_id'=>27
        ]);
        Feature::create([
          'name'=>'قابلیت های ویژه',
          'value'=>'رنگ آمیزی خاص و پر مفهوم بودن اثر',
          'product_id'=>27
        ]);
    }
}
