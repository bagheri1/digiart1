<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//landing page

use App\Http\Controllers\CheckoutController;

Route::get('/','LandingPageController@index')->name('landing-page');

//products
Route::get('/products','ShopController@index')->name('products');
Route::get('/product/{product}','ShopController@show')->name('product.show');

//Cart
Route::get('/cart','CartController@index')->name('cart.index');
Route::post('/cart','CartController@store')->name('cart.store');
Route::get('/cart/empty','CartController@empty')->name('cart.empty');
Route::delete('/cart/{product}','CartController@destroy')->name('cart.destroy');//save care for save For Later
Route::patch('/cart/{product}','CartController@update')->name('cart.update');
//save cart for later
Route::post('/cart/saveForLater/{product}','saveForLaterController@saveForLater')->name('cart.saveForLater');
Route::delete('/cart/deleteSaveForLater/{product}','saveForLaterController@destroy')->name('cart.destroySaveForLater');
Route::post('/cart/switchFromSavedToCart/{product}','saveForLaterController@switchFromSavedToCart')->name('cart.switchFromSavedToCart');


//checkout
Route::get('/checkout','CheckoutController@index')->name('checkout.index')->middleware('auth');
Route::post('/checkout','CheckoutController@store')->name('checkout.pay')->middleware('auth');
Route::get('/checkout/callback','CheckoutController@zarinPalCallBack')->name('checkout.callback')->middleware('auth');
Route::get('/checkout/successfull/{refid}','CheckoutController@successfull')->name('checkout.successfull');

//Coupons
Route::post('/Coupon','CouponController@store')->name('coupon.store');
Route::delete('/Coupon','CouponController@destroy')->name('coupon.destroy');


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//search
Route::get('/search','ShopController@Search')->name('search');
Route::get('autocomplete-ajax',array('as'=>'autocomplete.ajax','uses'=>'ShopController@ajaxSearchData'));


//customers

// Route::middleware('auth')->group(function(){
//     //if outhention is success
//     Route::get('/my-profile','UserController@edit')->name('user.edit');
//     Route::patch('/my-profile','UserController@update')->name('user.update');
// });

Route::group(['middleware' => 'auth'], function () {
    Route::get('/my-profile','UserController@edit')->name('user.edit');
    Route::patch('/my-profile','UserController@update')->name('user.update');
    Route::get('my-orders','UserController@orders')->name('user.orders') ;
    Route::get('/order-products/{id}','UserController@order_products')->name('order.products');
});


//voygercostomize
Route::get('/admin/order/products/{id}','HomeController@order_products')->name('action.show.order.products');
