@extends('layouts.layout-cart')

@section('cart')
  <div class="is-size-3">
    سبد خرید
  </div>
  @if (session()->has('success_message'))
    <div class="notification is-success">
        {{session()->get('success_message')}}
    </div>
  @endif

</div>
{{-- @if (session()->has('error_message'))
  <div class="notification is-dangers">
      {{session()->get('error_message')}}
  </div>
@endif --}}

 @if (count($errors)>0)
   @foreach ($errors->all() as $error)
     <li>{{$error}}</li>
   @endforeach
 @endif

  <hr>

  @if (Cart:: count()>0)
    <div class="columns">
        <div class="column da-cart-left-panel is-one-third">

            <table class="table is-fullwidth">
              <tr>
                <td>جمع سبد</td> <td>{{$newSubTotal}}تومان</td>
              </tr>
              <tr>
                <td>مالیات</td> <td>{{$newTax}}تومان</td>
              </tr>
              <tr class="has-text-link">
                  <td>جمع نهایی</td> <td>{{$newTotal}}تومان</td>
              </tr>
            </table>
            @if (session()->has('coupon'))
              <table class="table is-fullwidth has-text-danger">
                <tr>
                  <td>کد تخفیف</td>
                  <td>
                    <span>{{session()->get('coupon')['code']}}</span>
                    <form action="{{route('coupon.destroy')}}" method="post" style="display:inline;">
                      {{csrf_field()}}
                      {{method_field('delete')}}
                      <input type="submit" value="حذف">
                    </form>
                  </td>
                </tr>
                <tr>
                  <td>مقدار تخفیف</td><td>{{$discount}}</td>
                </tr>
              </table>
            @else
              <hr>
              <div class="">
                <form class="" action="{{route('coupon.store')}}" method="post">
                  {{csrf_field()}}
                  <input type="text" name="coupon_code" placeholder="کد تخفیف؟" class="input">
                  <button type="submit" class="button is-success is-fullwidth" name="button">اعمال کد تخفیف</button>
                </form>
              </div>        
           @endif
           <hr>
           <a href="{{route('checkout.index')}}" class="button is-success is-fullwidth"> ادامه ثبت سفارش </a>
        </div>

       <div class="column da-cart-right-panel">
         <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
            <tr>
              <td>تصویر</td>
              <td>نام</td>
              <td>جزییات</td>
              <td>قیمت</td>
              <td>تعداد</td>
              <td>عملیات</td>
            </tr>
            @foreach (Cart::content() as $item)
              <tr>
                <td>
                  <a href="{{route('product.show',$item->model->slug)}}">
                     <img width="150px" src="{{productImage($item->model->image)}}">
                  </a>
                </td>
                <td> {{$item->model->name}} </td>
                <td> {{$item->model->details}} </td>
                <td> {{$item->model->presentPrice($item->subtotal)}} </td>
                <td>
                    <div class="select is-rounded">
                        <select class="quantity" data-id="{{$item->rowId}}" >
                           @for ($i=1; $i < 5+1; $i++)
                             <option value="{{$i}}" {{$item->qty == $i?'selected':''}}>{{$i}}</option>
                           @endfor
                        </select>
                    </div>
                </td>
                <td>
                  {{-- delete one product from cart--}}
                  <form action="{{route('cart.destroy',$item->rowId)}}" method="post">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button type="submit" class="button is-danger is-outlined"><i class="fas fa-trash fa-1x"></i></button>
                  </form>
                  {{--save one product to later--}}
                  <form action="{{route('cart.saveForLater',$item->rowId)}}" method="post">
                    {{csrf_field()}}
                    <button type="submit" class="button is-info is-outlined" ><i class="fas fa-save fa-1x "></i></button>
                  </form>
                </td>
              </tr>
            @endforeach
          </table>
         </div>
     </div>
    <div class="">
      <hr>
      محصولات ذخیره شده:
      <hr>
      <table class=" table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
          @foreach (Cart::instance('saveForLater')->content() as $item)
            <tr>
              <td>
                <a href="{{route('product.show',$item->model->slug)}}">
                   <img width="150px" src="{{productImage($item->model->image)}}">
                </a>
              </td>
              <td> {{$item->model->name}} </td>
              <td> {{$item->model->details}} </td>
              <td> {{$item->model->price}} </td>
              <td> {{$item->qty}} </td>
              <td>
                {{-- delete one product from cart--}}
                <form action="{{route('cart.destroySaveForLater',$item->rowId)}}" method="post">
                  {{csrf_field()}}
                  {{method_field('DELETE')}}
                  <button type="submit" class="button is-danger is-outlined"><i class="fas fa-trash fa-1x"></i></button>
                </form>
                {{--save one product to later--}}
                <form action="{{route('cart.switchFromSavedToCart',$item->rowId)}}" method="post">
                  {{csrf_field()}}
                  <button type="submit" class="button is-info is-outlined" ><i class="fas fa-cart-plus fa-1x "></i></button>
                </form>
              </td>
            </tr>
          @endforeach
      </table>
    </div>
  @else
    <div class="empty-cart has-background-light has-text-centered">
        <i class="far fa-surprise fa-5x has-text-info"></i>
        <hr>
        <div class="is-size-3">
          سبد خرید شما خالی است
        </div>
        <hr>
        <a href="{{ route('login') }}" class="button is-info">
          ورود به حساب کاربری
        </a>
        <hr>
        <div class="">
          کاربر جدید هستید؟
          <a href="{{ route('register') }}" class="is-info">ثبت نام در دیجی آرت</a>
        </div>
        <hr>
        <a href="{{route('products')}}">  دیدن فروشگاه و ادامه خرید</a>

        <hr>
        محصولات ذخیره شده:
        <hr>
        <table class=" table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
            @foreach (Cart::instance('saveForLater')->content() as $item)
              <tr>
                <td>
                  <a href="{{route('product.show',$item->model->slug)}}">
                     <img width="150px" src="{{productImage($item->model->image)}}">
                  </a>
                </td>
                <td> {{$item->model->name}} </td>
                <td> {{$item->model->details}} </td>
                <td> {{$item->model->price}} </td>
                <td> {{$item->qty}} </td>
                <td>
                  {{-- delete one product from cart--}}
                  <form action="{{route('cart.destroySaveForLater',$item->rowId)}}" method="post">
                    {{csrf_field()}}
                    {{method_field('DELETE')}}
                    <button type="submit" class="button is-danger is-outlined"><i class="fas fa-trash fa-1x"></i></button>
                  </form>
                  {{--save one product to later--}}
                  <form action="{{route('cart.switchFromSavedToCart',$item->rowId)}}" method="post">
                    {{csrf_field()}}
                    <button type="submit" class="button is-info is-outlined" ><i class="fas fa-cart-plus fa-1x "></i></button>
                  </form>
                </td>
              </tr>
            @endforeach
        </table>
    </div>
  @endif
@endsection

@section('extra-js')
  <script src="{{asset('js/app.js')}}" charset="utf-8"></script>
  <script type="text/javascript">

  const classname = document.querySelectorAll('.quantity')

  Array.from(classname).forEach(function(element){

    const id = element.getAttribute('data-id')

    element.addEventListener('change',function(){

     axios.patch(`/cart/${id}`, {
         quantity : this.value
         })
        .then(function (response) {
          console.log(response);
           window.location.href = "{{route('cart.index')}}"
         })
         .catch(function (error) {
          console.log(error);
         });
            // let result;
          //  $.ajax({
          //     url:`/cart/${id}`,
          //     method: 'patch',
          //     data: {
          //       quantity : this.value,
          //       _token: $('input[name="_token"]').val()
          //     },
          //     async: false,
          //     success: function (response) {
          //       result = response;
          //       console.log("response: ", response);
                // window.location.href = "{{--{{route('cart.index')}}";--}}
          //     },
          //     error: function (xhr, status, error) {
          //         console.log("error: ", {xhr: xhr, status: status, error: error});
          //     }
          // });
          // console.log("result: ", result);
      });
    });

  </script>
@endsection

