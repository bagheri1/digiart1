@extends('layouts.layout-landing')

@section('products')
  <div class="columns is-multiline">
  @foreach ($products as $product)
      @include('layouts.product')
  @endforeach
  </div>
@endsection
