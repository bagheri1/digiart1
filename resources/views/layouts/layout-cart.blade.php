<!doctype html>

<html lang="fa">
<head>
  <meta charset="utf-8">

  <title>@yield('title')</title>

  <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
  <link rel="stylesheet" href="{{mix('/css/app.css')}}">
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>

<body>
    {{-- header of my application--}}
    @include('layouts.landing-header')
    <div class="container">
      @yield('cart')
    </div>
    {{--js files --}}
    @yield('extra-js')
    {{-- <script src="js/scripts.js"></script> --}}
</body>
</html>
