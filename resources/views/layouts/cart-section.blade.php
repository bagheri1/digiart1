<div class="buttons">
  <a class="button is-outlined is-da-green" href="{{route('cart.index')}}" >
    @if (Cart::instance('default')->count()>0)
      <strong>{{Cart::instance('default')->count()}}</strong>
    @endif
    <strong>سبد خرید</strong>
    <i class='fas fa-shopping-cart'></i>
  </a>
</div>
