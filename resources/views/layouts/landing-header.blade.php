<nav class="navbar navbar-digiart-header" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="{{URL('/')}}">
      <img src="https://www.digikala.com/static/files/bc60cf05.svg" width="112" height="10">
    </a>

    <a role="button" class="navbar-burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>

  </div>





  <div id="navbarMenu" class="navbar-menu">

    <div class="navbar-end">
      <div class="navbar-item has-dropdown is-hoverable">
        @guest
          <a class="navbar-link">
            ورود/ثبت نام
          </a>
        @else
          {{-- <div class="navbar-item has-text-danger">
             {{auth()->user()->name}}عزیز خوش آمدید
          </div> --}}
          <a class="navbar-link">
              پروفایل کاربری
          </a>
        @endguest


        <div class="navbar-dropdown da-navbar-dropdown">
          @guest
             <a class=" button is-da-green" href="{{route('login')}}">
                ورود به دیجی آرت
             </a>
             <div class="navbar-item">
                 <a href="{{route('register')}}" >ثبت نام</a>  کاربر جدید هستید؟
             </div>
          @else
             <a class="dropdown-item" href="{{route('user.orders')}}">
                <div class="navbar-item">
                    لیست سفارشات
                </div>
             </a>

          <div class="navbar-item">
                <a class="dropdown-item" href="{{ route('logout') }}"
                    onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                    {{ __('خروج') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </div>
          @endguest
          <hr class="navbar-divider">
          <a class="navbar-item" href="{{ route('user.edit') }}">
                 پروفایل کاربری <i class="fa fa-user fa-fw "></i>
          </a>
          <a class="navbar-item">
            پیگیری سفارش<i class="fas fa-shopping-bag fa-fw"></i>
          </a>

        </div>
      </div>
    </div>
    {{-- search box--}}

        <div class="navbar-end">
            <div class="navbar-item">
                <form action="{{route('search')}}" method="GET">
                  <div class="da-searchbox">
                   <div class="control has-icons-left has-icons-right is-fullwidth">
                     <input name="keyword" id="keyword" class="input is-fullwidth" type="text" placeholder="جستجو...">
                     <span class="icon is-small is-left is  is-da-red">
                         <button type="submit" class=" button is-danger fas fa-arrow-left has-text-white"></button>
                     </span>

                  </div>
                </form>
             </div>
             </div>
           </div>


    </div>

    <div class="navbar-start">
      <div class="navbar-item">
         @include('layouts.cart-section')
      </div>
    </div>
  </div>

</nav>


<nav class="navbar navbar-menu-da is-dark is right" role="navigation" aria-label="main navigation">
  <div class="navbar-start">
      @foreach ($categories as $category)
        <div class="navbar-item da-menu-item">
          <a class="has-text-white"
          href="{{route('products',['category'=>$category->slug])}}">
          {{$category->name}}
        </a>
        </div>
      @endforeach
  </div>
</nav>
<hr>

