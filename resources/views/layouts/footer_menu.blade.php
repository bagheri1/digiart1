<ul class="has-text-white">
    @foreach ($items as $menu_item)
    <li>
        <i class="fas fa-arrow-left" style="margin-bottom:10px"></i>
        <a class="has-text-white"|href="{{$menu_item->link()}}">{{$menu_item->title}}</a>
    </li>
    @endforeach
</ul>
