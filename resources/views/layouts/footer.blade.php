
<hr>
<footer class="footer has-background-dark has-text-black">
        <div class="columns has-text-centered">
            <div class="column">
                {{ menu('Footer.Left','layouts.footer_menu')}}
            </div>
            <div class="column">
              {{ menu('Footer.Middle','layouts.footer_menu')}}
            </div>
            <div class="column">
               {{ menu('Footer.Right','layouts.footer_menu')}}
            </div>
        </div>
</footer>
