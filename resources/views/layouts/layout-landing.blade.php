<!doctype html>

<html lang="fa">
<head>
  <meta charset="utf-8">
  {{-- <meta name="csrf-field" content="{{ csrf_field() }}"> --}}

  <title>{{setting('site.title')}} - {{setting('site.description')}}</title>

  <link rel="stylesheet" href="{{URL::asset('css/app.css')}}">
  <link rel="stylesheet" href="{{mix('/css/app.css')}}">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
</head>

<body>
{{-- header of my application--}}
@include('layouts.landing-header')
<div class="container">
  @yield('products')
</div>

{{--footer--}}
@include('layouts.footer')
{{--end footer--}}

  <script src="{{mix('/js/app.js')}}"></script>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>

    <script type="text/javascript">
        var url= "{{route('autocomplete.ajax')}}";
        $('#keyword').typeahead({
            source:function(query,process){
                return $.get(url,{query:query},function(data){
                    return process(data);
                });
            }
        });
    </script>

</body>
</html>
