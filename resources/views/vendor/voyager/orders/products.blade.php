@extends('voyager::master')

@section('page_title','محصولات سفارش')

@section('page_header')

@stop

@section('content')
    <h1 class="alert alert-primary" style="color:black">محصولات این سفارش:</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>تصویر</td>
                            <td>نام محصول</td>
                            {{-- <td>جزئیات محصول</td> --}}
                            <td>قیمت محصول</td>
                            <td></td>
                        </tr>
                        @foreach ($products as $product)
                        <tr>
                            <td><img width="100" src="{{productImage($product->image)}}" alt="placeholder image"></td>
                            <td>
                                {{$product->name}}
                                {{-- <a href="{{route('product.show',$product->slug)}}"> --}}
                                {{-- </a> --}}
                            </td>
                            {{-- <td>{{$product->details}}</td> --}}
                            <td>{{$product->presentPrice($product->price)}}تومان</td>
                            <td><a target="blank" href="{{route('product.show',$product->slug)}}"class="btn btn-success pull-right "> نمایش محصول</a></td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
        </div>
    </div>
    {{-- <h1>اطلاعات ارسال:</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>آدرس</td>
                            <td>{{$shipping->address}}</td>
                        </tr>
                        <tr>
                            <td>شماره تماس</td>
                            <td>{{$shipping->phone}}</td>
                        </tr>
                        <tr>
                            <td> کد پستی</td>
                            <td>{{$shipping->zipcode}}</td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <h1>اطلاعات پرداخت:</h1>
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-bordered">
                <div class="panel-body">
                    <table class="table">
                        <tr>
                            <td>رسید پرداخت</td>
                            <td>{{$payment->refid}}</td>
                        </tr>
                        <tr>
                            <td> مبلغ کل</td>
                            <td>{{$payment->price}}</td>
                        </tr>
                        <tr>
                            <td>وضعیت پرداخت</td>
                            <td> @if($payment->status == 'pending')
                              در انتظار
                              @elseif ($payment->status == 'complete')
                                پرداخت شده
                              @endif
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div> --}}

@stop

@section('css')

@stop

@section('javascript')

@stop
