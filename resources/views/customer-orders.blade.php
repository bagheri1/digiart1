@extends('layouts.layout-landing')

@section('products')
    <h2 class="is-size-4">سفارشات شما:</h2>
    <hr>
    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
       <tr>
           <td>شماره سفارش</td>
           <td>تاریخ سفارش</td>
           <td>تعدادمحصولات</td>
           <td></td>
       </tr>
       @foreach ($orders as $order)
         <tr>
             <td>{{$order->id}}</td>
             <td>{{$order->created_at}}</td>
             <td>
               @php

                 echo count (json_decode($order->product_ids));

                //   print_r(json_decode($order->product_ids)) ;
                //   $product_ids=json_decode($order->product_ids)[0];
                //   $product_array= explode(',',$product_ids);
                //   echo count($product_array);
               @endphp
             </td>
             <td>
                 <a href="{{route('order.products',$order->id)}}" class="button is-info">نمایش محصولات این سفارش</a>
             </td>
         </tr>
       @endforeach
   </table>
@endsection
