@extends('layouts.layout-product')
@section('product-details')
<br>
<div class="columns da-product">
  <div class="column auto">
    ویژگی های محصول<hr>
    <table class="table is-striped is-narrow is-hoverable is-fullwidth">
          @foreach ($features as $feature)
            <tr>
               <td class="da-features-name-td">{{$feature->name}}</td>
               <td class="is-size-7">{{$feature->value}}</td>
            </tr>
          @endforeach
    </table>
  </div>
  <div class="column is-half">
  <div>  {{$product->name}}  </div>
  <div class="is-size-8">   {{$product->details}}   </div>
  <br>
  <br>

  <div class="has-text-danger">{{$product->presentPrice($product->price)}}تومان</div>
  <div class="">
    <br>
     <form class="" action="{{route('cart.store')}}" method="post">
        {{csrf_field()}}
        {{-- product information --}}
          <input type="hidden" name="id" value="{{$product->id}}">
          <input type="hidden" name="name" value="{{$product->name}}">
          <input type="hidden" name="price" value="{{$product->price}}">
          <button type="submit" name="button" class="button is-da-green">افزودن به سبد خرید<i class="fas fa-shopping-cart fa-fw"></i></button>
     </form>

  </div>

  </div>
  <div class="column auto">
    {{-- <img src="{{asset()}}"> --}}
    <img src="{{productImage($product->image)}}">
  </div>
</div>
<hr>
<div class="columns da-product">
  <div class="column">
    {{$product->description}}
  </div>
  <div class="column is-one-third">
    <i class="fas fa-pen-fancy fa-3x has-text-danger"></i>
  </div>
</div>
<hr>
<div class="da-product">
  <div class="is-size-4">
 محصولاتی که ممکن است شما بپسندید:
  </div>
  <hr>
  <div class="columns">
    @foreach ($mightAlsoLike as $product)
       @include('layouts.product')
    @endforeach
  </div>
</div>

@endsection
