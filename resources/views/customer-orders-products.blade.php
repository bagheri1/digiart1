@extends('layouts.layout-landing')

@section('products')
    <h2 class="is-size-4">محصولات سفارش شماره {{$order_id}}</h2>
    <hr>
    <table class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth">
        <tr>
            <td>آیکون</td>
            <td>نام محصول</td>
            <td>جزئیات محصول</td>
            <td>قیمت محصول</td>


        </tr>
        @foreach ($products as $product)
        <tr>
            <td><img width="100" src="{{productImage($product->image)}}" alt="placeholder image"></td>
            <td>
                <a href="{{route('product.show',$product->slug)}}">
                    {{$product->name}}
                </a>
            </td>
            <td>{{$product->details}}</td>
            <td>{{$product->presentPrice($product->price)}}تومان</td>

        </tr>
        @endforeach


   </table>
@endsection
