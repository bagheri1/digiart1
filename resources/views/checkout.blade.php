@extends('layouts.layout-cart')

{{-- i didnot creat layout because  layout of --checkout-- is save with layout of --cart----}}
@section('cart')
<hr>
<div class="columns">
    <div class="column da-cart-left-panel">
        <div class="box">
            <div class="is-size-4 has-text-danger">
                لطفا اطلاعات تماس و آدرس خود را در زیر وارد کنید
            </div>
            <hr>
            <form action="{{route('checkout.pay')}}" method="POST">
                @guest
                نام خود را در انتهای آدرس بنویسید<br>
                @else
                نام تحویل گیرنده:{{auth()->user()->name}}<br>
                @endguest
                <table class="table is-fullwidth">
                    <tr>
                        <td><input class="input" type="number" name="phone" placeholder="شماره تماس" ></td>
                        <td><input class="input" type="number" name="zipcode" placeholder=" کد پستی" ></td>
                    </tr>
                    <tr>
                        <td colspan="2"> <input class="input" type="text" name="address" placeholder="آدرس دقیق" ></td>
                    </tr>
                </table>


        </div>
        <div class="box">
         محصولات مرسوله:
         <hr>
         <div class="columns is-multiline">
             @foreach (Cart::content() as $item)
             <div class="column">
                 <div class="cart" style="border:1px solid #d9d9d9;text-align:center;">
                     <img width="50px" src="{{asset('storage/'.$item->model->image)}}" alt="">
                     {{$item->model->name}}
                 </div>
             </div>
             @endforeach
         </div>
        </div>
    </div>
    <div class="column da-cart-right-panel is-one-third">

        <table class="table is-fullwidth">
            @csrf
          <tr>
            <td>جمع سبد</td> <td>{{$newSubTotal}}تومان</td>
          </tr>
          <tr>
            <td>مالیات</td> <td>{{$newTax}}تومان</td>
          </tr>
          <tr class="has-text-link">
              <td>جمع نهایی</td> <td>{{$newTotal}}تومان</td>
          </tr>
          <tr>
            @if (session()->has('coupon'))
            <table class="table is-fullwidth has-text-danger">

              <tr>
                <td>کد تخفیف</td>
                <td>
                  <span>{{session()->get('coupon')['code']}}</span>
                </td>
              </tr>
              <tr>
                <td>مقدار تخفیف</td><td>{{$discount}}</td>
              </tr>
            </table>
          @endif
          </tr>
        </table>
       <hr>

       <button type="submit"  class="button is-da-green is-fullwidth" style="padding:26px"> ادامه ثبت سفارش </button>
    </div>
</form>


</div>
@endsection
