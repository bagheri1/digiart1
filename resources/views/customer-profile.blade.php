@extends('layouts.layout-landing')

@section('products')
<section class="is-fullheight">
    <div class="hero-body">
        <div class="container has-text-centered">
            <div class="column is-4 is-offset-4">

                <h3 class="title has-text-grey"> <span class="has-text-grey">{{$user->name}}</span>  عزیز خوش آمدید </h3>
                <p class="subtitle has-text-grey">اطلاعات کاربری شما</p>

                @if (session()->has('msg'))
                    <div class="notification is-success">
                        {{session()->get('msg')}}
                    </div>
                @endif
                <div class="box">
                    <figure class="avatar">
                        <img width="100px" src="{{asset('img/register.png')}}">
                    </figure>
                    <form method="POST" action="{{ route('user.update') }}">
                        @method('patch')
                        @csrf
                        {{-- name --}}
                        <div class="field">
                            <div class="control">
                                <input placeholder="نام" id="name" type="text" class=" input is-large form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name' ,$user->name) }}" required autofocus>
                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- email --}}
                        <div class="field">
                            <div class="control">
                                <input placeholder="ایمیل" id="email" type="email" class="input is-large form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email'),$user->email }}" required>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- password --}}
                        <div class="field">
                            <div class="control">
                                <small>اگر نمیخواهید رمز تغییر پیدا کند این قسمت را خالی بگذارید</small>
                                <input placeholder="رمز" id="password" type="password" class="input is-large form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password">
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        {{-- password confirmation  --}}
                        <div class="field">
                            <div class="control">
                                <input placeholder="تکرار رمز" id="password-confirm" type="password" class="input is-large form-control" name="password_confirmation" >
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <button type="submit" class="button is-primary is-fullwidth">
                                ویرایش
                        </button>
                    </form>
                </div>
                <p class="has-text-grey">

                </p>
            </div>
        </div>
    </div>
</section>
@endsection
