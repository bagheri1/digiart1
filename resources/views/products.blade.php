@extends('layouts.layout-landing')

@section('products')

  <span class="has-text-grey has-text-weight-bold is-size-4">
  <br>
    {{$categoryName}}
  </span>
  <span>
  <span class="">مرتب سازی بر اساس:
  </span>
  <a href="{{route('products',['category'=>request()->category,'sort'=>'low_hight', 'page'=>$products->currentPage()])}}" class="tag is-link">ارزانترین</a> |
  <a href="{{route('products',['category'=>request()->category,'sort'=>'hight_low','page'=>$products->currentPage()])}}" class="tag is-link">گرانترین</a>
  </span>

  <hr>
  <div class="columns is-multiline">
  @foreach ($products as $product)
      @include('layouts.product')
  @endforeach
  </div>
  {{$products->appends(request()->input())->links()}}
@endsection
