@extends('layouts.layout-landing')

@section('products')
<hr>
  <div class="is-size-5 notification is-light">
      نتایج جستجو برای
      {{request()->input('keyword')}}:
      تعداد پیدا شده
      {{$products->total()}}
      مورد
  </div>
  <div class="columns is-multiline">
    @foreach ($products as $product)
        @include('layouts.product')
    @endforeach
  </div>
  {{$products->appends(request()->input())->links()}}
@endsection
